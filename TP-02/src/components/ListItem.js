import React, {Component} from 'react';

class ListItem extends Component {
  render() {
    return (
      <li key={this.props.key} className={this.props.done===true? 'done':null}>
        <p>{this.props.do}</p>
        <div className='allbutton'>
          <button className='btn'onClick={()=>this.props.onDone()}>{this.props.done===true?'✓':''}</button>
          <button className='btn'onClick={()=>this.props.onDelete()}>X</button>
        </div>
      </li>
    );
  }
}

export default ListItem;