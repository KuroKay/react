import React, {Component} from 'react';
import localStorage from 'localStorage';
import List from './List';

class Todo extends Component {
  constructor(props) {
    super(props);
    this.state={
      item: [
        {id: 1, do: 'Faire à manger', done: false},
        {id: 2, do: 'faire ma liste', done: false},
        {id: 3, do: 'salut', done: true},
      ],
    };
  }

  addItem() {
    const len = this.state.item.length;
    console.log(len);
    const inputDoOne = document.getElementsByTagName('input');
    const inputDo = inputDoOne[0];
    let newId = 0;
    if (len===0) {
      newId=0;
    } else {
      newId = (this.state.item[len-1].id)+1;
    }
    const newItem = [{id: newId, do: inputDo.value, done: false}];
    let itemCopy = this.state.item.slice();
    itemCopy = itemCopy.concat(newItem);
    this.setState({item: itemCopy});
    const val = JSON.stringify(itemCopy);
    localStorage.setItem('tableauListeTodo', val);
  }

  deleteItem(key) {
    const index = this.state.item.findIndex(i => i.id===key);
    const newT=this.state.item.slice();
    const newTab = newT.splice(index, 1);
    this.setState({item: newT});
  }

  done(key) {
    const index = this.state.item.findIndex(i=> i.id===key);
    const itemCopy = this.state.item.slice();
    if (this.state.item[index].done===false) {
      itemCopy[index].done = true;
    } else {
      itemCopy[index].done = false;
    }

    this.setState({item: itemCopy});
  }

  deleteAll() {
    let clearAll = this.state.item;
    clearAll = [];
    this.setState({item: clearAll});
  }

  render() {
    const value = JSON.parse(localStorage.getItem('tableauListeTodo'));
    return (
      <div>
        <h1>Liste ToDO</h1>
        <div className='groupe-input'>
          <input type='text' placeholder='A faire'/>
          <button onClick={()=>this.addItem()}>+</button>
        </div>
        
        <List item={this.state.item} onDelete={(key)=>this.deleteItem(key)} onDone={(key)=>this.done(key)}/>
        <div className='block__clearAll'>
          <button className='clearAll' onClick={()=>this.deleteAll()} >Clear All</button>
        </div>
      </div>
    );
  }
}

export default Todo;
