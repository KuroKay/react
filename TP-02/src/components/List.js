import React, {Component} from 'react';
import ListItem from './ListItem';

class List extends Component {
  render() {
    const itemsToDisplay = this.props.item.map(i=>{
      return (<ListItem key={i.id} do={i.do} done={i.done} onDelete={()=>this.props.onDelete(i.id)} onDone={()=>this.props.onDone(i.id)}/>);
    });
    return (
      <ul>
        {itemsToDisplay}
      </ul>
    );
  }
}

export default List;