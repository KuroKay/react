import React from 'react';
import ReactDOM from 'react-dom';
import Component from './components/todolist';
import './styles/global.scss';

const ReactRoot = document.getElementById('react-root');
ReactDOM.render(<Component />, ReactRoot);
