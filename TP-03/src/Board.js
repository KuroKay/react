import React, { Component } from "react";
import Snake from "./Snake";
import Apple from "./Apple";

//Board and Snake Direction
class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cooSnake: this.generatecooSnake(),
      direction: "TOP",
      cooApple: this.generatecooApple(),
      lost: false
    };
    document.addEventListener("keydown", this.changeDirection);
  }

  static defaultProps = {
    height: 600,
    width: 400
  };

  generatecooApple() {
    let x = Math.round((Math.random() * (this.props.height - 10)) / 10) * 10;
    let y = Math.round((Math.random() * (this.props.width - 10)) / 10) * 10;
    return [y, x];
  }

  generatecooSnake() {
    const x = Math.floor(this.props.height / 2);
    const y = Math.floor(this.props.width / 2);
    return [[y, x], [y, x - 10], [y, x - 20]];
  }

  componentDidMount() {
    this._speed = 150;
    this._intervalId = setInterval(() => {
      this.move(this.state.direction);
    }, this._speed);
  }

  componentWillUnmount() {
    clearInterval(this._intervalId);
    window.removeEventListener("keydown", this.changeDirection);
  }

  // unshift() add a or a lot of éléments in begining of a table and return th new length
  move(direction) {
    let cooApple = this.state.cooApple;
    let cooSnake = this.state.cooSnake;
    let head = cooSnake[0];
    // Add a head
    if (direction === "TOP") {
      if (head[0] === 0) {
        cooSnake.unshift([this.props.height - 10, head[1]]);
      } else {
        cooSnake.unshift([head[0] - 10, head[1]]);
      }
    }
    if (direction === "RIGHT") {
      if (head[1] === this.props.width - 10) {
        cooSnake.unshift([head[0], 0]);
      } else {
        cooSnake.unshift([head[0], head[1] + 10]);
      }
    }
    if (direction === "BOTTOM") {
      if (head[0] === this.props.height - 10) {
        cooSnake.unshift([0, head[1]]);
      } else {
        cooSnake.unshift([head[0] + 10, head[1]]);
      }
    }
    if (direction === "LEFT") {
      if (head[1] === 0) {
        cooSnake.unshift([head[0], this.props.width - 10]);
      } else {
        cooSnake.unshift([head[0], head[1] - 10]);
      }
    }

    this.setNewState(
      cooSnake,
      cooApple,
      this.lost(cooSnake),
      this._nextDirection
    );
  }

  setNewState(cooSnake, cooApple, lost, nextDirection) {
    /**
     * This conditional handles the business logic of snake movement
     * In the case that the apple and the head share coords, add to the score, increase speed, and don't pop off tail (this is how it grows)
     * Otherwise, pop off tail
     */
    if (this.sameCoords(cooApple, cooSnake[0])) {
      this.props.addScore();
      this.incrementSpeed(this.props.actuScore);
      cooApple = this.generatecooApple();
    } else {
      cooSnake.pop();
    }
    /**
     * Sets new direction if one has been set and sets it to null
     */
    let direction = this.state.direction;
    if (nextDirection) {
      direction = nextDirection;
      this._nextDirection = null;
    }

    this.setState({ cooSnake, cooApple, lost, direction });
    if (lost) {
      this.props.lostGame();
    }
  }

  lost(cooSnake) {
    for (let i = 1; i < cooSnake.length; i++) {
      if (this.sameCoords(cooSnake[0], cooSnake[i])) {
        return true;
      }
    }
    return false;
  }

  // accelerates the speed
  incrementSpeed(score) {
    this._speed = this._speed - 4;
    clearInterval(this._intervalId);
    this._intervalId = setInterval(() => {
      this.move(this.state.direction);
    }, this._speed);
  }

  sameCoords(coords1, coords2) {
    const [y1, x1] = coords1;
    const [y2, x2] = coords2;
    if (x1 === x2 && y1 === y2) {
      return true;
    }
    return false;
  }

  changeDirection = event => {
    event.stopPropagation();
    event.preventDefault();
    const key = event.keyCode;
    let direction = this.state.direction;
    if (key === 37 && direction !== "RIGHT") direction = "LEFT";
    if (key === 38 && direction !== "BOTTOM") {
      direction = "TOP";
    }
    if (key === 39 && direction !== "LEFT") direction = "RIGHT";
    if (key === 40 && direction !== "TOP") direction = "BOTTOM";
    this._nextDirection = direction;
  };

  render() {
    const board = (
      <div
        className="board"
        style={{
          height: `${this.props.height}px`,
          width: `${this.props.width}px`
        }}
      >
        <>
          <Snake parts={this.state.cooSnake} />
          <Apple coords={this.state.cooApple} />
        </>
      </div>
    );

    return this.state.lost ? null : board;
  }
}

export default Board;
