import React, { Component } from "react";
import "./Game.css";
import Board from "./Board";
import styled from "styled-components";

export default class App extends Component {
  constructor(props) {
    super(props);
    // Initalisation
    this.state = {
      lost: true,
      bestScore: null,
      actuScore: 0
    };
    this.lostGame = this.lostGame.bind(this);
    this.addScore = this.addScore.bind(this);
  }

  // Register Best Score
  componentDidMount() {
    let bestScore = localStorage.getItem("bestScore");
    this.setState({ bestScore });
  }

  // End GAme
  lostGame() {
    let bestScore = this.state.bestScore;
    let actuScore = 0;
    if (this.state.lost === false) {
      actuScore = this.state.actuScore;
    }
    if (this.state.actuScore >= this.state.bestScore) {
      bestScore = this.state.actuScore;
      localStorage.setItem("bestScore", bestScore);
    }
    this.setState(actuState => ({
      lost: !actuState.lost,
      bestScore,
      actuScore
    }));
  }

  // Incrémentation actuScore if snake eat an apple (5 points)
  addScore() {
    this.setState(actuState => ({ actuScore: actuState.actuScore + 5 }));
  }

  render() {
    const ButtonReplayModal = styled.div`
      background-color: orangered;
      position: absolute;
      z-index: 20000;
      top: 0;
      left: 0;
      width: 100vw;
      height: 100vh;
      color: white;
      display: flex;
      padding-top: 15vh;
      justify-content: center;
    `;
    return (
      <div className="page__snake">
        <div className="container">
          <div className="allScore">
            <div className="actuScore">Score: {this.state.actuScore}</div>
            <div className="bestScore">
              Meilleur Score: {this.state.bestScore ? this.state.bestScore : 0}
            </div>
          </div>
          {this.state.lost ? (
            <ButtonReplayModal>
              <div className="ModalBox">
                <h1>Snake Game</h1>
                <h2>
                  Meilleur Score:{" "}
                  {this.state.bestScore ? this.state.bestScore : 0}
                </h2>
                <button className="replay__button" onClick={this.lostGame}>
                  Rejouer
                </button>
              </div>
            </ButtonReplayModal>
          ) : (
            <Board
              height={this.props.height}
              width={this.props.width}
              actuScore={this.props.actuScore}
              addScore={this.addScore}
              lostGame={this.lostGame}
            />
          )}
        </div>
      </div>
    );
  }
}
