import React from 'react';
import ReactDOM from 'react-dom';
import Component from './components/default';
import ComponentUsers from './components/Users';
import './styles/global.scss';

const ReactRoot = document.getElementById('react-root');
const Compo = (<Component />, <ComponentUsers />);

ReactDOM.render( Compo, ReactRoot );
