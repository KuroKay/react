import React, {Component} from 'react';
import UserCard from './UserCard';

const API = 'https://randomapi.com/api/jbl8jr3b?key=D6KS-7OSA-IX7G-UFT3';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
      isLoading: true,
    };
  }
  componentDidMount() {
    fetch(API)
        .then(response => response.json())
        .then(data => {
          this.setState({users: data.results, isLoading: false});
          console.log(data.results);
        });
  }

  render() {
    const {users, isLoading} = this.state;
    if (isLoading) {
      return 'LOADING…';
    }
    return (
      <div className="users">
        {users.map(user => {
          return <UserCard key={user.id} user={user} />;
        })}
      </div>
    );
  }
}
export default App;
