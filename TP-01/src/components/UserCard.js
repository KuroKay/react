import React, {Component} from 'react';

class UserCard extends Component {
  handleClick = () =>{
    this.inputElement.click();
    const label = document.querySelector('label');
    label.classList.replace('post', 'new-post');
  }
  render() {
    return (
      <div className="users__item">
        <img className="users__picture" src={this.props.user.picture} />
        <h2 className="users__name">{this.props.user.name}</h2>
        <div className="groupe__input" onClick={this.handleClick}>
          <p ref={p => this.inputElement = p}><strong>Nom d'utilisateur:</strong></p>
          <label className="post">{this.props.user.username}</label>
        </div>
        <div className="groupe__input" onClick={this.handleClick}>
          <p ref={p => this.inputElement = p}><strong>Age:</strong></p>
          <label className="post">{this.props.user.year} ans</label>
        </div>
        <div className="groupe__input" onClick={this.handleClick}>
          <p ref={p => this.inputElement = p}><strong>Email:</strong></p>
          <label className="post">{this.props.user.email}</label>
        </div>
        <div className="groupe__input" onClick={this.handleClick}>
          <p ref={p => this.inputElement = p}><strong>Tél:</strong></p>
          <label className="post">{this.props.user.phone}</label>
        </div>
      </div>
    );
  }
}

export default UserCard;
