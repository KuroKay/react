import React, { Component } from "react";
import { BrowserRouter as Link } from "react-router-dom";

const API = "http://localhost:3000";

export default class blog extends Component {
  componentDidMount() {
    fetch(API)
      .then(response => response.json())
      .then(data => {
        this.setState({ data: data });
        console.log(this.state.data);
      })
      .catch(function(error) {
        console.log(
          "Il y a eu un problème avec l'opération fetch: " + error.message
        );
      });
  }

  render() {
    if (this.state === null) {
      return <div>loading...</div>;
    } else {
      return (
        <div>
          {this.state.data &&
            this.state.data.map((post, index) => (
              <div key={index}>
                <Link to="/post/:postId">
                  {post.id} {post.title} {post.description}
                </Link>
              </div>
            ))}
        </div>
      );
    }
  }
}
