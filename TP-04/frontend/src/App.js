import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import blog from "./ghibliBlog/AllPosts";
import PostPage from "./ghibliBlog/PostPage";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={blog} />
          <Route path="/post/:postId" component={PostPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
