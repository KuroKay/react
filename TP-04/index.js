const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
// Configuration de Express
const PORT = 3000;
const posts = require("./posts.json");

// app.use(express.static("build"));
app.use(morgan("combined"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
// Déclaration des routes
app.get("/", (req, res) => {
  res.send("Hello world ! ");
});
app.get("/posts", (req, res) => {
  res.send(JSON.stringify(posts));
});
app.get("/posts/:id", (req, res) => {
  const { id } = req.params;
  const post = posts.find(p => p.id === parseInt(id));
  res.send(JSON.stringify(post));
});

app.post("/", (req, res) => {
  res.json({ message: "Hello World " });
});

app.post("/posts", (req, res) => {
  const post = req.body;
  posts.push(post);
  res.json(posts);
});

app.delete("/user", function(req, res) {
  res.send("Got a DELETE request at /user");
});
// Lancement du serveur Web
app.listen(PORT, () => console.log(`Serveur lancé sur le port ${3000}`));
